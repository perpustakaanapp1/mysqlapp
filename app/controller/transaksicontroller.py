from flask.helpers import safe_join
from app.models.transaksimodels import Database_transaksi
from app.models.transaksimodels import *
from flask import jsonify, request
from flask_jwt_extended import *
import json, datetime

transaksi = Database_transaksi()

def tampilkantransaksibyid(param):
    dbresult = transaksi.showTransaksiByUserId(param)
    result = []
    for items in dbresult:
        transaksidata = {
            "idtransaksi" : items.idtransaksi,
            "total": items.total,
            "iduser" : items.iduser,
            "idproduk" :items.idproduk
        }
        result.append(transaksidata)
    return jsonify(result)

def addTransaksi(**param):
    transaksi.insertTransaksi(**param)
    data = {
        "message":"Data Transaksi Berhasil di tambahkan"
    }
    return jsonify(data)

def hapustransaksi(param):
    transaksi.deleteTransaksi(param)
    data={
        "Message":"Transaksi Berhasil Dihapus"
    }
    return jsonify(data)

def tampilkansmuatransaksi():
    dbresult = transaksi.showalltransaksi()
    result = []
    for items in dbresult:
        transaksidata = {
            "idtransaksi" : items.idtransaksi,
            "total": items.total,
            "iduser" : items.iduser,
            "idproduk" :items.idproduk
        }
        result.append(transaksidata)
    return jsonify(result)