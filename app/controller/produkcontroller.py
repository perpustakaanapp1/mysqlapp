from flask.helpers import safe_join
from app.models.produkmodels import Database_produk
from app.models.produkmodels import *
from flask import jsonify, request
from flask_jwt_extended import *
import json, datetime

mysqlcrudproduk = Database_produk()

#Code Untuk Menampilkan data produk
def tampilall():
    dbresult = mysqlcrudproduk.showProduk()
    result = []
    for items in dbresult:
        produk = {
            "idproduk" : items.idproduk,
            "nama" : items.nama,
            "price" : items.price,
            "nama_toko" : items.nama_toko,
            "stock" : items.stock
        }
        result.append(produk)
    return jsonify(result)

#Insert Data Produk
def masukproduk(**param):
    mysqlcrudproduk.insertProduk(**param)
    data = {
        "message":"Data Produk Berhasil di masukkan"
    }
    return jsonify(data)

#update Data Produk
def updateproduk(param):
    mysqlcrudproduk.updateProduk(param)
    data = {
        "message":"Data Produk Berhasil di Ubah"
    }
    return jsonify(data)

#hapus data Produk
def hapusproduk(param):
    mysqlcrudproduk.deleteProduk(param)
    data = {
        "message":"Data Produk Berhasil Dihapus"
    }
    return jsonify(data)

#tampilkan berdasarkan nama toko
def produkpertoko(param):
    dbresult = mysqlcrudproduk.showProdukByToko(param)
    result = []
    for items in dbresult:
        produk = {
            "idproduk" : items.idproduk,
            "nama" : items.nama,
            "price" : items.price,
            "nama_toko" : items.nama_toko,
            "stock" : items.stock
        }
        result.append(produk)
    return jsonify(result)