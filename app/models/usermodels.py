from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import relationship
from sqlalchemy import *
from sqlalchemy.orm import backref

 
#connection ke database
Base = declarative_base()
engine = create_engine('mysql+mysqlconnector://root:Asaulter123@localhost:3306/final_project', echo=True)
Session = sessionmaker(bind=engine)
session = Session()

if session:
    print("Connection Success")

#class untuk table
class User(Base):
    __tablename__ = 'user'
    iduser = Column(Integer, primary_key =  True, autoincrement= True, nullable = False )
    email = Column(String, nullable = False)
    password = Column(String, nullable = False)
    nama_lengkap = Column(String)
    gender = Column(String)
    no_telp = Column(String)

#crud data user
class Database_user():
    def showUser(self):
        try:
            result = session.query(User).all()
            return result
        except Exception as e:
            print(e)
        
    def showUserByEmail(self, email):
        try:
            result = session.query(User).filter(User.email == email)
            return result
        except Exception as e:
            print(e)
    
    def insertUser(self, **param):
        try:
            session.add(User(**param))
            session.commit()
        except Exception as e:
            print(e)
    def showUserById(self, Id):
        try:
            result = session.query(User).filter(User.iduser == Id)
            return result
        except Exception as e:
            print(e)
    def deleteUserById(self, Id):
        try:
            result = session.query(User).filter(User.iduser == Id).one()
            session.delete(result)
            session.commit()
        except Exception as e:
            print(e)
    def updateUserById(self, data):
        try:
            result = session.query(User).filter(User.iduser == data['iduser']).one()
            result.email = data['email']
            result.password = data['password']
            result.nama_lengkap = data['nama_lengkap']
            result.gender = data['gender']
            result.no_telp = data['no_telp']
            session.commit()
        except Exception as e:
            print(e)